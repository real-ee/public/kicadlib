LV [0-5V]
----
* Resistor:
	* 1005				0				1/16W	|	MF-RES-0402-0
	* 1005				1kΩ		5%		1/16W	|	MF-RES-0402-1K
	* 1005				10kΩ	5%		1/16W	|	MR-RES-0402-10k
	* 1005				100kΩ	5%		1/16W	|	MF-RES-0402-100K
	* 1005				1MΩ		5%		1/16W	|	MR-RES-0402-1M
	* 1005 x2 CVX		33­­Ω		5%		1/16W	|	EXB-34V330JV
	* 1005 x4 CVX		1kΩ		5%		1/16W	|	YC124-JR-071KL
	* 1005 x4 CVX		10kΩ	5%		1/16W	|	YC124-JR-0710KL
	* 1608				60.4Ω	1%		1/4W	|	CRCW060360R4FKEAHP

* Capacitor:
	* 1005				100nF	10V+	X6R+	|	MF-CAP-0402-0.1uF		16V X7R
	* 1005				1uF		10V+	X6R+	|	GRM155C81E105KE11D		25V X6S
	* 1608				10uF	10V+	X6R+	|	GRM188C81C106MA73D		16V X6S

* Inductor:
	* 2012				4.7uH	0.5A+	SHD		|	LQM21PN4R7MGRD			0.8A
	* 2520				2.2uH	1A+		SHD		|	CVH252009-2R2M			1.3A

* Ferrite:
	* 1005				1kΩ+	0.25A+			|	BK1005HS102-T			0.3A
	* 1608				100Ω+	2A+				|	BLM18KG221SN1D			220Ω 2.2A

* PTC:
	* 1608				0.2A	6V+				|	0ZCM0020FF2G			9V
	* 1608				0.5A	6V+				|	MF-FSMF050X-2

* LED:
	* REERGB			STD RGB pixel			|	LTST-E263CEGBK

HV [0-28V]
----
* Diode:
	* SOD-123			50V+	0.5A			|	MF-DIO-SOD123-1N4148	100V

* TVS:
	* DFN380X100X75-9	3.3V	8 uni			|	D3V3F8U9LP3810-7

* Transistor:
	* SOT-23			P-Ch	50V+	0.1A+	|	MF-DSC-SOT233-BSS84		0.13A
	* SOT-23			N-Ch	50V+	0.1A+	|	MF-DSC-SOT233-BSS138	0.22A		
	* SOT-563			2x N-Ch	50V+	0.1A+	|	DMG1026UV-7
	* SOT-563			2x P-Ch	50V+	0.1A+	|	DMP58D0SV-7
	* SOT-563			N/P-Ch	50V+	0.1A+	|	DMG1029SV-7

MEC
----
* Connector:
	* External connection to harness:				Molex_5007620481
	* Header:
		* 2x5 50mil TH vert						|	GRPB052VWVN-RC
		* 2x5 50mil TH horz						|	
		* 2x10 50mil TH vert					|	
	* USB:
		* Micro B TH horz						|	MF-MICROUSB

* Button:
	* Tact:
		* small vertical SMD					|	MF-SW-TACT-4.2MM