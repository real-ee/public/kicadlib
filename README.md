# KiCADlib

This is a parts library for [KiCAD][kicad_link] containing schematic symbols, PCB footprints, 3D models, part's documentations and others utils for HW design.
Currently compatible with developpement branch of version 6, aka [nithgly][kicaddl_link] or [5.99][kicaddl_link], refer to the [docs][kicaddoc_link] for differences between versions

## Organization

- _[doc][doc_folder]_: All type of documentation ie: datasheets, manuals, reference guide, drawings, etc.
- _[footprint][footprint_folder]_: Standard and custom PCB footprints grouped by type inside subfolder
- _[model][model_folder]_: 3D models used in the footprints
- _[symbol][symbol_folder]_: Schematic symbol available in this lib
- _[template][template_folder]_: Projects templates to fast-tracking new projects
- _[tool][tool_folder]_: Different tools for typical hw design work

## Install

1. clone the [repo][repo_link] to a local folder accessible to KiCAD
2. create an [environment variable][kicad_path_link] inside KiCAD named `KICADLIB` pointing to the root folder of the repo
3. [install][kicad_libinstall_link] the necessary symbol lib from the folder [symbol][symbol_folder]
4. [install][kicad_libinstall_link] the necessary footprint lib from the folder [footprint][footprint_folder]

## Contribution

Contribution can be made following thoses principles:

- Parts added should be complete and ready to use, thus must include:
  - a symbol, see [Symbol](#Symbol-creation) section for details
  - one or multiple footprint, see [Footprint](#Footprint-creation) section for details
  - some sort of relevant documentation, see [Doc](#Doc-to-include) section for details
- Every element (symbol, footprint, etc) should adhere to its respective requirements
- Part's name should follow [IPC-7351B][ipc7351b_file] naming convention or its principle

----

### Symbol creation

Schematic should be as readable and understandable as possible, thus symbols should follow the following guidelines:

- **Name** should:
  - be as much generic as possible (ie: ATSAMV70Qxxx-C not ATSAMV70Q19A-C)
  - follow the current naming scheme for similar parts
- **Drawing** should:
  - be representative of the part
    - use standard representation if available
    - use generic rectangular box for complex device
  - should be horizontally biased (no top and bot pins, unless typical symbol needs it)
  - be as readable as possible, account for pin names and other specificities
  - be centered on both axis (mind the pin alignment to the grid, slight vertical offset is acceptable)
  - include useful and static information (schematic clarity is paramount, i2c addresses are ok, voltage ranges less so)
- **Pins** should:
  - all be of the _same length_ (long enough for the widest pin designator)
  - have a lenght that is a _multiple of 1.27mm_
  - have the appropriate _electrical type_ (follow datasheet information)
  - have the appropriate _graphical style_
  - be _aligned_ to the default 1.27mm grid
  - be _organised_ for typical schematic usage (think of the typical schematic around the part)
    - ground(s) should be placed toward bottom
  - be _named_ according to datasheet using available [decorators][kicad_textdeco_link]
  - be _present_ only if an electrical connection may be needed (no NC)
  - include _central pads_ and other atypical connection point
    - if a specific net is supposed to be connected apprend to the name in `()`
  - be _grouped_ if multiple pin must be connected to same net
    - exception can be made for specific schematic requirements (ie: dedicated pin for specific decoupling scheme)
- **Metadata** should:
  - have a generic but [standard][refdes_link] **reference** designator _[visible]_
  - set a useful **value**, to differentiate the parts or include specifics (ie: resistance for a resistor) _[visible]_
  - include the correct or typical (if multiple) **footprint** _[not visible]_
    - this footprint should exist in the [footprint][footprint_folder] folder
  - link the _top-level_ specific product page as **datasheet** (whatchout for metatag in url) _[not visible]_
  - include at least one valid _orderable part-number_ as **MPN** (hopefully in supply -_-) _[visible]_
    - additionnal valid (same footprint) _MPNs_ should be added as new metadata **MPN2**, **MPN3**, etc.
  - include a **DNP** metadata set to `0` _[not visible]_
    - if the part should not be populated (ie: virtual), set to `DNP`
- **Multi-part** should:
  - be used if multiple similar elements are present (ie: dual op-amp should use 2)
  - be minimized for "too large to fit" parts and sectionned in a relevant manner (io banks, power pins, etc)


### Footprint creation

To create a valid footprint all those rules should be observed:

- **Designator** shoud be present:
  - on silkscreen as the magic `REF**` used by KiCAD, centered on top of part's body (must be visible)
  - on fab as a substituted symbol `${REFERENCE}` _centered_ and _within_ the body outline
    - text should use [default](#Footprint-defaults) value if possible, but can be reduced to fit inside outline
- **Outline** should be marked:
  - on silkscreen as much of the body as possible, especially if the corner can be drawned.
  - on fab with the complete and continuous body, if multiple sub-parts are important, they should be drawn too
  - on all layers, lines should be centered on the actual dimension and using [default](#Footprint-defaults) values is possible
- **Pins** should be:
  - all present and named according to datasheet
  - include relevant soldermask opening and paste mask
  - use simple geometry if possible (ie: rectangle is better than rounded rectangle)
- **Pin 1** should be marked:
  - on silkscreen by a `^` pointing toward the pin
  - on fab by a notch on the part outline
- **Courtyard** should be defined as a continuous rectangle emcompassing all the _pcb-area_ used and the relevant tolerances
- **Placement Anchor** should be centered in both axis
  - exception exist for connectors, _pcb-area_ should be centered in that case
- **3D model** should:
  - all be present and spacially positioned as it would be assembled on real PCB (only 1 should be visible by default)
  - colored in a realistic way and using [default](#Footprint-defaults) colors if possible
  - include some realistic representation of the orientation marking (follow real parts physical aspect)
  - be a [step][stepfile_link] file placed in the [model][model_folder] folder, linked using the `KICADLIB` env. var
  - be a single and continuous 3D body (no space between sub-section or multi-section assembly)
  - be as simple as needed for mechanical verification and realistic presentation (ie: fine details should be avoided, less is more)

#### Footprint defaults

- **lines**:
  - silkscreen: 0.1mm
  - fab: 0.1mm
  - courtyard: 0.05mm
- **text**:
  - width: 1mm
  - height: 1mm
  - thickness: 0.15mm
- **3D colors**:
  - typical black plastic body: `#1a1a1a`
  - tinned metal: `#cecece`
  - gold metal: `#ffd93e`

### Doc to include

Depending on the type of parts, different documentation is required:

- ICs should include:
  - datasheet
  - family guide/manual if it exist or is referenced in the datasheet for some information
  - errata if relevant
  - drawing if not already included in previous doc
- Passives should include:
  - family datasheet **OR** specific part datasheet
- Connectors should include:
  - complete mechanical drawing
  - datasheet if it exists
  - footprint drawing if not already present in previous doc and it exist
- Mechanical
  - complete mechanical drawing
  - datasheet if it exists
  - assembly instruction if it exists
- Virtual/Images
  - source image if it exists

Please do not add reference documentation directly, send a request and the document to [maintainer][maintainer_email]

### Adding templates

As of now **(2021-10-19)** no special requirements are specified, simply follow the [guide][kicad_template_link].

## License

Just [Ask][maintainer_email]


[repo_link]:                    https://gitlab.com/real-ee/public/kicadlib
[kicad_link]:                   https://www.kicad.org/
[kicaddl_link]:                 https://www.kicad.org/download/
[kicaddoc_link]:                https://docs.kicad.org/master/en/kicad/kicad.html
[kicad_path_link]:              https://docs.kicad.org/master/en/kicad/kicad.html#paths_configuration
[kicad_libinstall_link]:        https://docs.kicad.org/master/en/kicad/kicad.html#libraries-configuration
[kicad_template_link]:          https://docs.kicad.org/master/en/kicad/kicad.html#project-templates
[kicad_textdeco_link]:          NO-OFFICIAL-DOC-YET
[freecad_link]:                 https://www.freecadweb.org/
[stepfile_link]:                https://en.wikipedia.org/wiki/ISO_10303-21
[refdes_link]:                  https://en.wikipedia.org/wiki/Reference_designator

[maintainer_email]:             mailto:laurence@realee.tech

[ipc7351b_file]:                doc/reference/IPC-7351B%20Naming%20Convention.pdf

[doc_folder]:                   doc/
[footprint_folder]:             footprint/
[model_folder]:                 model/
[symbol_folder]:                symbol/
[template_folder]:              template/
[tool_folder]:                  tool/
